package com.example.university2;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DBHelper  extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 2;
    public static final String DATABASE_NAME = "interviewDb";
    public static final String TABLE_INTERVIEW = "interview";

    public static final String KEY_ID = "_id";
    public static final String KEY_NAME = "name";
    public static final String KEY_PURPOSE = "purpose";
    public static final String KEY_FUNCTIONAL = "functional";
    public static final String KEY_REQUIREMENTS = "requirements";
    public static final String KEY_LANGUAGE = "language";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table " + TABLE_INTERVIEW + "(" + KEY_ID
                + " integer primary key," + KEY_NAME + " text," + KEY_PURPOSE + " text," +
                KEY_FUNCTIONAL + " text," + KEY_REQUIREMENTS + " text," +
                KEY_LANGUAGE + " text" +")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table if exists " + TABLE_INTERVIEW);

        onCreate(db);
    }
}