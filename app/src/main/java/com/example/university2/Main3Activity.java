package com.example.university2;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.constraint.Constraints;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;

public class Main3Activity extends AppCompatActivity {
    DBHelper dbHelper;
    LinearLayout linearLayout1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main3);

        linearLayout1 = (LinearLayout) findViewById(R.id.linearLayout1);


        dbHelper = new DBHelper(this);
        SQLiteDatabase database = dbHelper.getWritableDatabase();

        Cursor cursor = database.query(DBHelper.TABLE_INTERVIEW,
                null, null, null,
                null, null, null);

        if (cursor.moveToFirst()) {
            int idIndex = cursor.getColumnIndex(DBHelper.KEY_ID);
            int nameIndex = cursor.getColumnIndex(DBHelper.KEY_NAME);
            int purposeIndex = cursor.getColumnIndex(DBHelper.KEY_PURPOSE);
            int functionalIndex = cursor.getColumnIndex(DBHelper.KEY_FUNCTIONAL);
            int requirementsIndex = cursor.getColumnIndex(DBHelper.KEY_REQUIREMENTS);
            int languageIndex = cursor.getColumnIndex(DBHelper.KEY_LANGUAGE);

            do {
                String msg = "ID = " + cursor.getInt(idIndex) +
                        ", name = " + cursor.getString(nameIndex) +
                        ", purpose = " + cursor.getString(purposeIndex) +
                        ", functional = " + cursor.getString(functionalIndex) +
                        ", requirements = " + cursor.getString(requirementsIndex) +
                        ", language = " + cursor.getString(languageIndex);

                Log.d("readDatabaseLog", msg);
                TextView textView1 = new TextView(this);
                textView1.setLayoutParams(new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT,
                                Constraints.LayoutParams.WRAP_CONTENT));
                textView1.setText(msg);
                textView1.setPadding(20, 20, 20, 20);
                linearLayout1.addView(textView1);
            } while (cursor.moveToNext());
        } else
            Log.d("mLog","0 rows");

        cursor.close();
        dbHelper.close();
    }
}
