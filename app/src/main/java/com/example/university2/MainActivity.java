package com.example.university2;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements OnClickListener {
    Button btnMain2Activity, btnMain3Activity, btnDeleteRecords;
    DBHelper dbHelper;
    private static final String TAG = "main";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "MainActivity создано.");
        setContentView(R.layout.activity_main);

        btnMain2Activity = findViewById(R.id.btnMain2Activity);
        btnMain2Activity.setOnClickListener(this);

        btnMain3Activity = findViewById(R.id.btnMain3Activity);
        btnMain3Activity.setOnClickListener(this);

        btnDeleteRecords = findViewById(R.id.btnDeleteRecords);
        btnDeleteRecords.setOnClickListener(this);

        dbHelper = new DBHelper(this);
    }
    @Override
    public void onClick(View v) {

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        switch (v.getId()) {
            case R.id.btnMain2Activity:
                Intent intent1 = new Intent(this, Main2Activity.class);
                startActivity(intent1);
                break;
            case R.id.btnMain3Activity:
                Intent intent2 = new Intent(this, Main3Activity.class);
                startActivity(intent2);
                break;
            case R.id.btnDeleteRecords:
                database.delete(DBHelper.TABLE_INTERVIEW, null, null);
                break;
            default:
                break;
        }
        dbHelper.close();
    }
}
