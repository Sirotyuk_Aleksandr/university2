package com.example.university2;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;

public class Main2Activity extends AppCompatActivity implements View.OnClickListener{
    Button buttonSave;
    EditText etRequirements, etFunctional, etPurpose, etName;
    RadioGroup radioGroup1;
    String language;

    DBHelper dbHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        radioGroup1 = (RadioGroup) findViewById(R.id.radioGroup1);

        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(this);

        etName = (EditText) findViewById(R.id.etName);
        etPurpose = (EditText) findViewById(R.id.etPurpose);
        etFunctional = (EditText) findViewById(R.id.etFunctional);
        etRequirements = (EditText) findViewById(R.id.etRequirements);

        dbHelper = new DBHelper(this);

        radioGroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton1:
                        language = "Java";
                        break;
                    case R.id.radioButton2:
                        language = "C++";
                        break;
                    case R.id.radioButton3:
                        language = "Kotlin";
                        break;
                    case R.id.radioButton4:
                        language = "Python";
                        break;
                    default:
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        String name = etName.getText().toString();
        String purpose = etPurpose.getText().toString();
        String functional = etFunctional.getText().toString();
        String requirements = etRequirements.getText().toString();

        SQLiteDatabase database = dbHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();


        switch (v.getId()) {
            case R.id.buttonSave:
                contentValues.put(DBHelper.KEY_NAME, name);
                contentValues.put(DBHelper.KEY_PURPOSE, purpose);
                contentValues.put(DBHelper.KEY_FUNCTIONAL, functional);
                contentValues.put(DBHelper.KEY_REQUIREMENTS, requirements);
                contentValues.put(DBHelper.KEY_LANGUAGE, language);

                database.insert(DBHelper.TABLE_INTERVIEW, null, contentValues);
                break;
        }
        dbHelper.close();
    }
}
